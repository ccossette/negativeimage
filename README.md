#**Instructions**
Build a JAR file using `sbt assembly`. The file will be located in the `NegativeImage/target/scala-2.12` directory once completed.

To create the negative of a 24-bit BGR bitmap image, run the following command from terminal:
```java -cp NegativeImage.jar negativeImage.NegativeImageCli -i <path/to/input/file.bmp>  -o <path/for/the/outputFile.bmp>```

You can access the help section using the command:
```java -cp NegativeImage.jar negativeImage.NegativeImageCli --help```

In the project folder, you will find photos that were used as part of unit/integration tests. All photos were formatted using GIMP to be 24-bit bitmap files, and `grumpyNegativeFish.bmp` was created as a negative of the `grumpyFish.bmp` image in GIMP to verify to validity of the negative images produced in code. 

Note to devs: Other processing/filter functions can be added to the main function and passed as a parameter to the `runThrough` function.