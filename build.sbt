import AssemblyKeys._

assemblySettings

name := "NegativeImage"

version := "1.0"

scalaVersion := "2.12.4"

libraryDependencies ++= {
  Seq(
    "org.rogach"          %% "scallop"      % "3.1.1",
    "org.scalatest"       %% "scalatest"    % "3.0.5" % Test
  )
}

jarName in assembly := "NegativeImage.jar"