
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import negativeImage.{NegativeImageCli, UnsupportedFileException}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import scala.util.Random

class NegativeTest extends FlatSpec with Matchers with BeforeAndAfterAll {

  val outFile = new File("test1.bmp")

  override def afterAll = {
    if (outFile.exists) {
      outFile.delete()
    }
  }

  "RunThrough test" should "return the correct negative image" in {
    val image = ImageIO.read(new File("grumpyFish.bmp"))
    val expected = ImageIO.read(new File("grumpyNegativeFish.bmp"))
    val negative = NegativeImageCli.runThrough(NegativeImageCli.negative, image)
    ImageIO.write(negative, "bmp", outFile)

    val x = new Random().nextInt(image.getWidth)
    val y = new Random().nextInt(image.getHeight)

    negative.getRGB(x, y) shouldBe expected.getRGB(x, y)
  }

  "One-pixel image test" should "create a negative of the initial colour" in {
    val r = (new scala.util.Random).nextInt(256)
    val g = (new scala.util.Random).nextInt(256)
    val b = (new scala.util.Random).nextInt(256)
    val col = (r<<16)|(g<<8)|b
    var image = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR)
    image.setRGB(0,0,col)

    val newR = 255 - r
    val newG = 255 - g
    val newB = 255 - b
    val newCol = (newR<<16)|(newG<<8)|newB
    var newImage = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR)
    newImage.setRGB(0,0,newCol)

    val neg = NegativeImageCli.runThrough(NegativeImageCli.negative, image)

    neg.getRGB(0, 0) shouldBe newImage.getRGB(0, 0)
  }

  "Failing Test" should "Output exception when given the wrong file resolution" in {
    val arguments = Array("-i", "badGrumpyFish.bmp", "-o", "output.bmp")
    val thrown = intercept[UnsupportedFileException] {
      NegativeImageCli.main(arguments)
    }
    assert(thrown.getMessage === "Image not supported.")
  }
}
