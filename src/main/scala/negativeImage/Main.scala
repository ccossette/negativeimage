package negativeImage

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import org.rogach.scallop.ScallopConf

import scala.util.Try

final case class UnsupportedFileException(
  private val message: String = "Unsupported file type, only 24-bit BMP files accepted(TYPE_3BYTE_BGR).",
  private val cause: Throwable = None.orNull
) extends Exception(message, cause)

class Conf(args: Array[String]) extends ScallopConf(args){
  banner("""Usage: java -cp NegativeImage.jar negativeImage.NegativeImageCli {inputFile} {outputFile}
    |Options:
    |""".stripMargin)
  version("NegativeImage Version 1.0 (c) 2018 C Cossette")
  footer("\nFor all other inquiries, please contact me!")

  val inputFile = opt[String](
    required = true,
    descr = "The filename of the input file, including its filepath"
  )
  val outputFile = opt[String](
    required = true,
    descr = "The filename of the output file, including its filepath"
  )
  verify()
}

object NegativeImageCli {

  case class JobDescription (
    inputFile: String,
    outputFile: String
  )

  def main(args: Array[String]): Unit = {
    val fromCli = new Conf(args)

    val Some(inputFile) = fromCli.inputFile.toOption
    val Some(outputFile) = fromCli.outputFile.toOption

    val img = Try {
      val file = new File(inputFile)
      ImageIO.read(file)
    }.recover {
      case e: Exception => throw e
    }.get

    //Check if Image is the right format
    val imageType = img.getType
    if(imageType == 5 && inputFile.endsWith("bmp")){
      //Output File
      val outFile = new File(outputFile)
      val newImage = runThrough(negative, img)
      ImageIO.write(newImage, "bmp", outFile)

    } else { throw new UnsupportedFileException("Image not supported.")}
  }

  /*
   *  Based on the dimensions of the input image, a new image is created.
   *  Each pixel is then accessed and the parameter function is applied to the pixel's colour code.
   */

  def runThrough(f: Int => Int, image: BufferedImage): BufferedImage = {
    //Find size of image
    val width = image.getWidth()
    val height = image.getHeight()

    //New image, same size
    var newImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR)

    //Iterate through pixels and change pixel colour of new image
    val r = for {
      y <- 0 until height
      x <- 0 until width
    } yield newImage.setRGB(x, y, f(image.getRGB(x,y)))

    newImage
  }

  /*
   * Shift bits for red, green, and blue components and find their opposite value.
   * Add all three components together to create the new colour code.
   */
  def negative(pix: Int): Int = {
    var r = (pix>>16)&0xff
    var g = (pix>>8)&0xff
    var b = pix&0xff
    r = 255 - r
    g = 255 - g
    b = 255 - b
    (r<<16)|(g<<8)|b
  }
}
